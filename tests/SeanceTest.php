<?php
//importation de la Classe utile TestCase - avec un namespace:
use PHPUnit\Framework\TestCase;
 
//importer les Class utiles:
include_once(__DIR__ ."/../models/Seance.php");
include_once(__DIR__ ."/../models/User.php");
include_once(__DIR__ ."/../models/Database.php");

//Creation d'une classe qui etend la classe TestCase (pour ajouter une fonction test):
final class SeanceTest extends TestCase{

    //ajout/creation de la fonction test:
    public function testCreateSeance(){
        //appel de la fonction static createSeance de la Class Seance + insertion de valeurs tests
        $seance = Seance::createSeance("Acrobatie", "Ce cours améliore la coordination générale, la souplesse et la condition physique", "12:00", date("Y-m.d"), 45, 16, " #ff951d", "tenu de gym, basket", 15);
        //créer une new database pour faire une connexion à la base de données
        $database = new Database();
        //"si la Seance est insérée dans la base, je n'ai pas false" =
        $this->assertNotFalse($database->createSeance($seance));
    }
    public function testGetSeanceById(){
        $database = new Database;
        //creation d'une seance
        $seance = Seance::createSeance("Acrobatie", "Ce cours améliore la coordination générale, la souplesse et la condition physique", "18:00", date("Y-m.d"), 45, 16, " #ff951d", "tenu de gym, basket", 15);
        //récupération de l'id de la séance créée
        $id = $database->createSeance($seance);
        //Certification de l'existance de la séance
        $this->assertInstanceOf(Seance::class, $database->getSeanceById($id));
    }
    public function testGetSeanceByWeek(){
        $database = new Database;
        //créer une séance à la date d'aujourd'hui
        $seance = Seance::createSeance("Acrobatie", "Ce cours améliore la coordination générale, la souplesse et la condition physique", "10:00", date("Y-m.d"), 45, 16, " #ff951d", "tenu de gym, basket", 15);
        //je vérifie la création de la séance
        $this->assertNotFalse($database->createSeance($seance));
        //je compte le nb de seances avec le numero de la semaine
        $nbSeances = count($database->getSeanceByWeek(date("W")));
        echo($nbSeances);
        //je vérifie qu'il y en a au moin une
        $this->assertGreaterThan(0, $nbSeances);
    }
    public static function tearDownAfterClass(){
        $database = new Database;
        $database->deleteAllInscrit();
        $database->deleteAllUser();
        $database->deleteAllSeance();
    }
    public function testUpdateSeance(){
        $database = new Database;
        $seance = Seance::createSeance("Acrobatie", "Ce cours améliore la coordination générale, la souplesse et la condition physique", "15:00", date("Y-m.d"), 45, 16, " #ff951d", "tenu de gym, basket", 15);
        //récupération de l'id de la séance créée
        $id = $database->createSeance($seance);
        //récupère la séance
        $seance = $database->getSeanceById($id);
        $this->assertInstanceOf(Seance::class, $seance);
        //Modification de la séance
        $seance->setHeureDebut("14:30");
        //vérification de la mise à jour
        $this->assertTrue($database->updateSeance($seance));
    }
    public function testDeleteSeance(){
        $database = new Database();
        $seance = Seance::createSeance("Acrobatie", "Ce cours améliore la coordination générale, la souplesse et la condition physique", "17:00", date("Y-m.d"), 45, 16, " #ff951d", "tenu de gym, basket", 15);
        $id = $database->createSeance($seance);
        $this->assertTrue($database->deleteSeance($id));
    }
    public function testInsertDeleteParticipant(){
        $database = new Database;
        //créer un user, sauvegarde et vérification
        $user = User::createUser("Samy", "samylaki@gmail.com", password_hash("1234", PASSWORD_DEFAULT), 0, 0, bin2hex(random_bytes(20)));
        $idUser = $database->createUser($user);
        $this->assertNotFalse($idUser);
        //créer une séance, sauvegarde et vérification
        $seance = Seance::createSeance("Athletisme", "Ce cours est destiné à des niveaux avancés en athletisme", "18:00", date("Y-m.d"), 90, 8, " #ff957b", "tenu d'athletisme", 35);
        $idSeance = $database->createSeance($seance);
        $this->assertNotFalse($idSeance);
        //inserer un participant
        $this->assertTrue($database->insertParticipant($idSeance, $idUser));
        //supprimer le participant
        $this->assertTrue($database->deleteParticipant($idSeance, $idUser));
    }

    public function testGetSeanceByUserId(){
        $database = new Database;
        $user = User::createUser("Johny", "jokojok@hotmail.ch", password_hash("1234", PASSWORD_DEFAULT), 0, 0, bin2hex(random_bytes(20)));
        $idUser = $database->createUser($user);
        $this->assertNotFalse($idUser);
        /////vérifie la récupération d'un tableau de séances prévues VIDE
        $this->assertEquals(0, count($database->getSeanceByUserId($idUser)));
        //Crée une séance
        $seance1 = Seance::createSeance("GYM", "cours de gym", "17:00", date("Y-m-d"), 45, 20, "#03bafc", "tenu d'athletisme", 35);
        //récupérer l'id de la séance crée
        $idSeance1 = $database->createSeance($seance1);
        $this->assertNotFalse($idSeance1);
        //inscription du user à la séance crée
        $this->assertTrue($database->insertParticipant($idSeance1, $idUser));
        //Vérifier que le tableau d'une seance pour ce user
        $this->assertEquals(1, count($database->getSeanceByUserId($idUser)));
        //créer une 2e séance
        $seance2 = Seance::createSeance("Jumps", "entraînement aux sauts", "21:00", date("Y-m-d"), 90, 12, "#07cdga", NULL, 10);
        $idSeance2 = $database->createSeance($seance2);
        $this->assertNotFalse($idSeance2);
        $this->assertTrue($database->insertParticipant($idSeance2, $idUser));
        $this->assertEquals(2, count($database->getSeanceByUserId($idUser)));
    }
}
