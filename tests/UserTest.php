<?php
//importation de la Classe utile TestCase - avec un namespace:
use PHPUnit\Framework\TestCase;
 
//importer les Class utiles:
include_once(__DIR__ ."/../models/Seance.php");
include_once(__DIR__ ."/../models/User.php");
include_once(__DIR__ ."/../models/Database.php");

//Creation d'une classe qui etend la classe TestCase (pour ajouter une fonction test):
final class UserTest extends TestCase{

     //ajout/creation de la fonction test:
     public function testCreateUser(){
        //appel de la fonction static createUser de la Class User + insertion de valeurs tests 
        //(0 pour non-admin et 0 pour non-actif sans validation par email)
        //token: chaine hexadécimale pour générer la validation d'insciption - random_bytes les génère
        $user = User::createUser("Jason", "jasonlooms@gmail.com", password_hash("1234", PASSWORD_DEFAULT), 0, 0, bin2hex(random_bytes(20)));
        //créer une new database pour faire une connexion à la base de données
        $database = new Database();
        //"si la Seance est insérée dans la base, je n'ai pas false" =
        $this->assertNotFalse($database->createUser($user));
    }
    /* public function testDeleteUser(){
        $database = new Database();
        $user = User::createUser("Micheal", "mikizooms@gmail.com", password_hash("1234", PASSWORD_DEFAULT), 0, 0, bin2hex(random_bytes(20)));
        $id = $database->createUser($user);
        $this->assertTrue($database->deleteUser($id));
    } */
    
    public static function tearDownAfterClass(){
        $database = new Database;
        $database->deleteAllInscrit();
        $database->deleteAllUser();
        $database->deleteAllSeance();
    }
    public function testAndActivateUser(){
        $database = new Database;
        $user = User::createUser("Jimmy", "jimsgohome@devmen.com", password_hash("1234", PASSWORD_DEFAULT), 0, 0, bin2hex(random_bytes(20)));
        $id = $database->createUser($user);
        $this->assertNotFalse($id);
        //ACTIVER
        $this->assertTrue($database->activateUser($id));
        //récupérer
        $user = $database->getUserById($id);
        $this->assertInstanceOf(User::class, $user);
        //vérifier
        $this->assertEquals(1, $user->isActif());
    }
    public function testEmailAlreadyExists(){
        $database = new Database;
        $user = User::createUser("Mike", "minimills@outlook.com", password_hash("1234", PASSWORD_DEFAULT), 0, 0, bin2hex(random_bytes(20)));
        $this->assertNotFalse($database->createUser($user));
        //Vérification Email exists
        $emailTrue = "minimills@outlook.com";
        $this->assertTrue($database->isEmailExists($emailTrue));
        //Vérification Email NotExists
        $emailFalse = "minimills@outlook.com";
        $this->assertFalse($database->isEmailExists($emailFalse));
    }

    public function testGetUserByEmail(){
        $database = new Database;
        //creation du user
        $user = User::createUser("Mike", "minimills@outlook.com", password_hash("1234", PASSWORD_DEFAULT), 0, 0, bin2hex(random_bytes(20)));
        //insérer et vérifier
        $this->assertNotFalse($database->createUser($user));
        //vérifier le lien user-email
        $emailTrue = "minimills@outlook.com";
        $this->assertInstanceOf(User::class, $database->getUserByEmail($emailTrue));
        //Si pas d'Email, retourne personne
        $emailFalse = "minimills@outlook.com";
        $this->assertFalse($database->getUserByEmail($emailFalse));
    }
}
