<?php
include_once 'Seance.php';
include_once 'User.php';

/* Classe de connexion à la base de Donnée */
/* const pour données constantes */
class Database
{
    const DB_HOST = "mariadb";
    const DB_PORT = "3306";
    const DB_NAME = "clubOmega";
    const DB_USER = "adminClub";
    const DB_PASSWORD = "qwertz1234";

    /* Attribut de la classe */
    /* variable qui stocke la connexion établie */
    private $connexion;

    /* Constructeur */
    /* initialisation de la connexion en faisant appel au constructeur de la classe PDO */
    public function __construct()
    {
        try {
            $this->connexion = new
                PDO(
                    "mysql:host=" . self::DB_HOST . ";port=" . self::DB_PORT . ";dbname=" . self::DB_NAME . ";charset=UTF8",
                    self::DB_USER,
                    self::DB_PASSWORD
                );
        } catch (PDOException $e) {
            echo 'Connexion échouée : ' . $e->getMessage();
        }
    }
    /* GESTION DES SEANCES DANS LA BASE DE DONNEES ET DE SES FONCTION RELATIVES */
    /**
     * Creer une nouvelle seance dans la base de données
     * @param{Seance} seance : la nouvelle séance à sauvegarder
     * @return{integer, boolean} le nouvel id si la séance est crée OU 'false' si c'est pas le cas
     */
    public function createSeance(Seance $seance)
    {
        //PREPARER la requête SQL : grâce à la connexion, la requête prépare SQL à insérer des données dans une table précise avec les valeurs mentionnées relatives
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO seances(titre, description, heureDebut, date, duree, nbParticipantsMax, couleur, materiel, tarif)
              VALUES (:titre, :description, :heureDebut, :date, :duree, :nbParticipantsMax, :couleur, :materiel, :tarif)"
        );
        //EXECUTER la requête : grâce à la classe Seance (requierée au début du document), je récupère les informations du nouvel objet
        $pdoStatement->execute([
            "titre"                    => $seance->getTitre(),
            "description"              => $seance->getDescription(),
            "heureDebut"               => $seance->getHeureDebut(),
            "date"                     => $seance->getDate(),
            "duree"                    => $seance->getDuree(),
            "nbParticipantsMax"        => $seance->getNbParticipantsMax(),
            "couleur"                  => $seance->getCouleur(),
            "materiel"                 => $seance->getMateriel(),
            "tarif"                    => $seance->getTarif()
        ]);
        //RECUPERER le nouvel ID crée (new Seance) grâce à la connexion - si l'execution est réussi (code 0000 de MySQL)
        if ($pdoStatement->errorCode() == 0) {
            $id = $this->connexion->LastInsertId();
            return $id;
        } else {
            /* var_dump($pdoStatement->errorInfo()); pour récuperer le detail*/
            return false;
        }
    }
    /**
     * Cherche la séance dont l'id est en paramètre:
     * @param{integer} id : l'id de la séance recherchée'
     * @return{Seance, boolean} un objet Seance OU 'false' si c'est pas le cas
     */
    public function getSeanceById($id)
    {
        $pdoStatement = $this->connexion->prepare(
            //selectionner toutes les entrées dans la tables seances qui respectent l'id recherché
            "SELECT * FROM seances WHERE id = :id"
        );
        $pdoStatement->execute(
            //exécuter la requête en lui donnant l'id recherché : dans l'index id(les ids) le id recherché.
            ["id" => $id]
        );
        //récupère la séance qui correspond à la recherche qui choppe l'objet "Seance"
        $seance = $pdoStatement->fetchObject("Seance");
        return $seance;
    }
    /**
     * Retourner toutes les séances de la semaine
     * @param{integer} week: le numero de la semaine
     * @return{array} tableau : des séances programmées
     */
    public function getSeanceByWeek($week)
    {
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM `seances` WHERE WEEKOFYEAR(date) = :week
            ORDER BY date, heureDebut"
        );
        //lui donner le numero de la semaine
        $pdoStatement->execute(
            ["week" => $week]
        );
        //récupérer le résultat - fonction existante de PDO (?voir doc)
        $seances = $pdoStatement->fetchAll(PDO::FETCH_CLASS, "Seance");
        return $seances;
    }
    /* public function getSeanceByTitle($titre)
    {
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM `seances` WHERE titre = :titre
            ORDER BY titre, date"
        );
        //lui donner le numero de la semaine
        $pdoStatement->execute(
            ["titre" => $titre]
        );
        //récupérer le résultat - fonction existante de PDO (?voir doc)
        $seances = $pdoStatement->fetchAll(PDO::FETCH_CLASS, "Seance");
        return $seances;
    } */
    public function deleteAllSeance()
    {
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM seances;"
        );
        $pdoStatement->execute();
    }
    public function updateSeance($seance)
    {
        $pdoStatement = $this->connexion->prepare(
            "UPDATE seances
            SET titre = :titre, description = :description, heureDebut = :heureDebut,
                date = :date, duree = :duree, nbParticipantsMax = :nbParticipantsMax,
                couleur = :couleur, materiel = :materiel, tarif = :tarif
                WHERE id = :id"
        );
        $pdoStatement->execute([
            "titre"                    => $seance->getTitre(),
            "description"              => $seance->getDescription(),
            "heureDebut"               => $seance->getHeureDebut(),
            "date"                     => $seance->getDate(),
            "duree"                    => $seance->getDuree(),
            "nbParticipantsMax"        => $seance->getNbParticipantsMax(),
            "couleur"                  => $seance->getCouleur(),
            "materiel"                 => $seance->getMateriel(),
            "tarif"                    => $seance->getTarif(),
            "id"                       => $seance->getId()
        ]);
        //Avoir un vérificateur "true" si tout c'est bien passé
        if ($pdoStatement->errorCode() == 0) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * Retourner une seance de la base
     * @param{integer} id de la seance à supprimer
     * @return{boolean} true si elle est supprimée, false sinon
     */
    public function deleteSeance($id)
    {
        //D'abord supprimer tous les inscrits de la séance qui disparait
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM inscrits WHERE id_seance = :seance"
        );
        $pdoStatement->execute(
            ["seance" => $id]
        );
        //doit avoir la confirmation avant la suite
        if ($pdoStatement->errorCode() != 0) {
            return false;
        }
        //preparation suppression seance
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM seances WHERE id = :seance"
        );
        $pdoStatement->execute(
            ["seance" => $id]
        );
        //verification
        if ($pdoStatement->errorCode() == 0) {
            return true;
        } else {
            return false;
        }
    }
    public function insertParticipant($idSeance, $idUser)
    {
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO inscrits (id_user, id_seance) VALUES (:id_user, :id_seance)"
        );
        $pdoStatement->execute(
            [
                "id_user" => $idUser,
                "id_seance" => $idSeance
            ]
        );
        if ($pdoStatement->errorCode() == 0) {
            return true;
        } else {
            return false;
        }
    }
    public function deleteParticipant($idSeance, $idUser)
    {
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM inscrits 
            WHERE id_user = :id_user 
            AND id_seance = :id_seance"
        );
        $pdoStatement->execute(
            [
                "id_user" => $idUser,
                "id_seance" => $idSeance
            ]
        );
        if ($pdoStatement->errorCode() == 0) {
            return true;
        } else {
            return false;
        }
    }
    public function isInscritExists($idUser, $idSeance){
        $pdoStatement = $this->connexion->prepare(
            "SELECT COUNT(*) FROM inscrits 
             WHERE id_user = :id_user
             AND id_seance = :id_seance"
        );
        $nbInscrit = $pdoStatement->fetchColumn();
        if ($nbInscrit == 0) {
            return true;
        } else {
            return false;
        }
    }
 /*    public function maxInscrits($idSeance, $nbParticipantsMax){
        $pdoStatement = $this->connexion->prepare(
            "SELECT COUNT(*) FROM inscrits
            WHERE id_seance = :id_seance
            AND nb_participantsMax = :nbParicipantsMax"
        );
        $nbInscrits = $pdoStatement->fetchColumn();
        if($nbInscrits >= $nbParticipantsMax){
            return true;
        }else{
            return false;
        }
    } */


    /* GESTION DES UTILISATEURS DANS LA BASE DE DONNES ET SES FONCTIONS RELATIVES */
    /**
     * Creer un nouvel utilisateur dans la base de données
     * @param{User} user : le nouvel user à sauvegarder
     * @return{integer, boolean} le nouvel id si le user est crée OU 'false' si c'est pas le cas
     */
    public function createUser(User $user)
    {
        //PREPARER la requête SQL : grâce à la connexion, la requête prépare SQL à insérer des données dans une table précise avec les valeurs mentionnées relatives
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO users(nom, email, password, isAdmin, isActif, token)
             VALUES (:nom, :email, :password, :isAdmin, :isActif, :token)"
        );
        $pdoStatement->execute([
            "nom"           => $user->getNom(),
            "email"         => $user->getEmail(),
            "password"      => $user->getPassword(),
            "isAdmin"       => $user->isAdmin(),
            "isActif"       => $user->isActif(),
            "token"         => $user->getToken()
        ]);
        //RECUPERER le nouvel ID crée (new User) grâce à la connexion - si l'execution est réussi (code 0000 de MySQL)
        if ($pdoStatement->errorCode() == 0) {
            $id = $this->connexion->LastInsertId();
            return $id;
        } else {
            return false;
        }
    }
    public function updateUser($user)
    {
        $pdoStatement = $this->connexion->prepare(
            "UPDATE users
            SET nom = :nom, /* email = :email, password = :password, isAdmin = :isAdmin, isActif = isActif, token = :token */
             WHERE id = :id"
        );
        $pdoStatement->execute([
            "nom"                    => $user->getNom(),
            "email"                  => $user->getEmail(),/* 
            "password"               => $user->getPassword(),
            "isAdmin"                => $user->isAdmin(),
            "isActif"                => $user->isActif(),
            "token"                  => $user->getToken() */
        ]);
        //Avoir un vérificateur "true" si tout c'est bien passé
        if ($pdoStatement->errorCode() == 0) {
            return true;
        } else {
            return false;
        }
    }
    public function deleteAllUser()
    {
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM users;"
        );
        $pdoStatement->execute();
    }
    public function deleteAllInscrit()
    {
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM inscrits;"
        );
        $pdoStatement->execute();
    }
    /**
     * Récupérer un user avec un id
     * @param{integer} id : le nouvel user à sauvegarder
     * @return{User, boolean} le user OU 'false' si c'est pas le cas
     */
    public function getUserById($id)
    {
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM users WHERE id = :id"
        );
        $pdoStatement->execute(
            ["id" => $id]
        );
        $user = $pdoStatement->fetchObject("User");
        return $user;
    }
    /**
     * Activer un nouvel utilisateur dans la base de données
     * @param{integer} id : le nouvel user à sauvegarder
     * @return{boolean} le user OU 'false' si c'est pas le cas
     */
    public function activateUser($id)
    {
        $pdoStatement = $this->connexion->prepare(
            "UPDATE users
            SET isActif = 1
            WHERE id = :id"
        );
        $pdoStatement->execute(
            ["id" => $id]
        );
        if ($pdoStatement->errorCode() == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Vérifier que l'email soit unique
     * @param{string} email : l'email du user
     * @return{boolean} le nombre de fois qu'il apparait dans la table user
     * false = 0(email trouvé), true = l'email existe
     */
    public function isEmailExists($email)
    {
        $pdoStatement = $this->connexion->prepare(
            "SELECT COUNT(*) FROM users WHERE email = :email"
        );
        $pdoStatement->execute(
            ["email" => $email]
        );
        //Récupérer le résultat
        $nbUser = $pdoStatement->fetchColumn();
        //Réponse
        if ($nbUser == 0) {
            return false;
        } else {
            return true;
        }
    }
    /**
     * Retrouver le user par l email (connexion utilisateur actif)
     * @param{string} email : l'email du user
     * @return{User | boolean} le User ou false
     */
    public function getUserByEmail($email)
    {
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM users
            WHERE email = :email"
        );
        $pdoStatement->execute(
            ["email" => $email]
        );
        $user = $pdoStatement->fetchObject("User");
        return $user;
    }
    /**
     * Retrouver les séances à laquelles le user est inscrit, depuis son id
     * @param{integer} id : l'id du user
     * @return{array} le tableau des seances
     */
    public function getSeanceByUserId($idUser)
    {
        $pdoStatement = $this->connexion->prepare(
            "SELECT s.* FROM seances s INNER JOIN inscrits i ON s.id = i.id_seance WHERE i.id_user = :id_user"
        ); //s, i : alias seances, inscrits
        $pdoStatement->execute(
            ["id_user" => $idUser]
        );
        $seances = $pdoStatement->fetchAll(PDO::FETCH_CLASS, "Seance");
        return $seances;
    }
}