<?php

// classe de base, avec des propriétés et des méthodes membres
class User
{

    private $id;
    private $nom;
    private $email;
    private $password;
    private $isAdmin;
    private $isActif;
    private $token;

    /* constructeur de base */
    public function __construct()
    {
    }

    /* creation */
    public static function createUser($nom, $email, $password, $isAdmin, $isActif, $token)
    {
        $user = new self();
        $user->setNom($nom);
        $user->setEmail($email);
        $user->setpassword($password);
        $user->setAdmin($isAdmin);
        $user->setActif($isActif);
        $user->setToken($token);
        return $user;
    }

    /* getters */
    public function getId()
    {
        return $this->id;
    }
    public function getNom()
    {
        return $this->nom;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function getPassword()
    {
        return $this->password;
    }
    public function isAdmin()
    {
        return $this->isAdmin;
    }
    public function isActif()
    {
        return $this->isActif;
    }
    public function getToken()
    {
        return $this->token;
    }

    /* setters */
    public function setId($id)
    {
        return $this->id = $id;
    }
    public function setNom($nom)
    {
        return $this->nom = $nom;
    }
    public function setEmail($email)
    {
        return $this->email = $email;
    }
    public function setPassword($password)
    {
        return $this->password = $password;
    }
    public function setAdmin($isAdmin)
    {
        return $this->isAdmin = $isAdmin;
    }
    public function setActif($isActif)
    {
        return $this->isActif = $isActif;
    }
    public function setToken($token)
    {
        return $this->token = $token;
    }
}
