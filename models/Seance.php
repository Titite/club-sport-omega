<?php

// classe de base, avec des propriétés et des méthodes membres
class Seance
{

    private $id;
    private $titre;
    private $description;
    private $heureDebut;
    private $date;
    private $duree;
    private $nbParticipantsMax;
    private $couleur;
    private $materiel;
    private $tarif;

    /* constructeur de base */
    public function __construct()
    {
    }

    /* creation */
    public static function createSeance($titre, $description, $heureDebut, $date, $duree, $nbParticipantsMax, $couleur, $materiel, $tarif)
    {
        $seance = new self();
        $seance->setTitre($titre);
        $seance->setDescription($description);
        $seance->setHeureDebut($heureDebut);
        $seance->setDate($date);
        $seance->setDuree($duree);
        $seance->setNbParticipantsMax($nbParticipantsMax);
        $seance->setCouleur($couleur);
        $seance->setMateriel($materiel);
        $seance->setTarif($tarif);
        return $seance;
    }

    /* getters */
    public function getId()
    {
        return $this->id;
    }
    public function getTitre()
    {
        return $this->titre;
    }
    public function getDescription()
    {
        return $this->description;
    }
    public function getHeureDebut()
    {
        return $this->heureDebut;
    }
    public function getDate()
    {
        return $this->date;
    }
    public function getDuree()
    {
        return $this->duree;
    }
    public function getNbParticipantsMax()
    {
        return $this->nbParticipantsMax;
    }
    public function getCouleur()
    {
        return $this->couleur;
    }
    public function getMateriel()
    {
        return $this->materiel;
    }
    public function getTarif()
    {
        return $this->tarif;
    }

    /* setters */
    public function setId($id)
    {
        return $this->id = $id;
    }
    public function setTitre($titre)
    {
        return $this->titre = $titre;
    }
    public function setDescription($description)
    {
        return $this->description = $description;
    }
    public function setHeureDebut($heureDebut)
    {
        return $this->heureDebut = $heureDebut;
    }
    public function setDate($date)
    {
        return $this->date = $date;
    }
    public function setDuree($duree)
    {
        return $this->duree = $duree;
    }
    public function setNbParticipantsMax($nbParticipantsMax)
    {
        return $this->nbParticipantsMax = $nbParticipantsMax;
    }
    public function setCouleur($couleur)
    {
        return $this->couleur = $couleur;
    }
    public function setMateriel($materiel)
    {
        return $this->materiel = $materiel;
    }
    public function setTarif($tarif)
    {
        return $this->tarif = $tarif;
    }
}
