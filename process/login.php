<?php
    session_start();
    require_once(__DIR__ ."/../models/Database.php");
    $database = new Database;

    $email = isset($_POST["email"]) ? $_POST["email"] : null;
    $password = isset($_POST["password"]) ? $_POST["password"] : null;
    

    if($email == null || $password == null){
        $_SESSION["error"] = "L'email et le mot de passe sont obligatoire";
        header("location: ../vues/login.php");
     }

     //recuperation du user
     $user = $database->getUserByEmail($email);
     if(!$user){
        $_SESSION["error"] = "L'email est incorrect, vous n'avez pas été trouvé";
      header("location: ../vues/login.php");
      exit();
     }

     if($user->isActif() == 0){
        $_SESSION["error"] = "Votre compte n'a pas encore été validé, consultez vos emails";
        header("location: ../vues/login.php");
        exit();
     }
     if(!password_verify($password, $user->getPassword())){
        $_SESSION["error"] = "Le mot de passe est incorrect";
        header("location: ../vues/login.php");
        exit();
     }

     //Si tout ok, connecter le user : ajout Id et objet User dans la session; info, redirection
     $_SESSION["id"] = $user->getId();
     $_SESSION["user"] = serialize($user); //$user en mode chaine de caracteres
     $_SESSION["info"] = "Vous êtes bien connecté";
     header("location: ../vues/planning.php");