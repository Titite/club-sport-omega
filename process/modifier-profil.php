<?php
//////ACTION POUR LE SERVEUR - PROCESSER LES DONNEES
///TRANSFERER LES MESSAGES PAGES-PAGES
//DEBUT DES PAGES
session_start(); //démarrage de la session(?quelle session?)
require_once(__DIR__ . "/../models/Database.php");
$database = new Database(); //pour futures interactions avec BDD

/* RECUPERER DONNEES BDD */
$user = $database->getUserById($id);
$nomOrigine = $user->getNom();

/* RECUPERER DONNES DU FORMULAIRE */
$nom = isset($_POST["nom"]) ? $_POST["nom"] : null;
$id = isset($_POST["id"]) ? $_POST["id"] : null;

/* CHAMPS BIEN REMPLI */
$errors = "";
if ($nom == null || $nom == $nomOrigine) {
    $errors .= "Aucune modification n'a été faite";
}
//en cas d'erreur - REDIRIGER vers le formulaire
if (!empty($errors)) {
    $_SESSION["error"] = $errors;
    header("location: ../vues/modifier-profil.php");
    exit();
}
/* RESULTAT */
$error = null;
$succes = null;
if ($database->updateUser($user)) {
    $succes = "Le profil a été modifié avec succès";
} else {
    $error = "La modification de votre profil a rencontré une erreur";
}

//////REDIRECTIONS
if ($error == null) {
    //tout ok - message ok - direction page vues seance avec la nouvelle affichée
    $_SESSION["info"] = $succes;
    header("Location: ../vues/profil.php?id=" . $id);
} else {
    $_SESSION["info"] = $error;
    //redirection du surfer vers le formulaire malrempli
    header("Location: ../vues/modifier-profil.php?id" . $id . "&type=" . $type);
}
