<?php
    session_start();

    require_once(__DIR__ ."/../models/Database.php");
    $database = new Database;
 /* var_dump($_POST); */
    //recuperation donnees
   $nom = isset($_POST["name"]) ? $_POST["name"] : null;
   $email = isset($_POST["email"]) ? $_POST["email"] : null;
   $password = isset($_POST["password"]) ? $_POST["password"] : null;
   $passwordRepeat = isset($_POST["password-repeat"]) ? $_POST["password-repeat"] : null;
/* echo ($password); */

$errors = "";
if($nom == null){
    $errors .= "Le nom d'utilisateur doit être rempli";
 }
if($email == null){
    $errors .= "L'email doit être renseigné";
 }
if($database->isEmailExists($email)){
    $errors .= "Cet email existe déjà";
 }
if($password == null){
    $errors .= "Le password est obligatoire";
 }
if($passwordRepeat == null || $passwordRepeat != $password){
    $errors .= "Vous devez répéter le même mot de passe";
 }
//en cas d'erreur - REDIRIGER vers le formulaire
 if(!empty($errors)){
    $_SESSION["error"] = $errors;
    header("location: ../vues/inscription.php");
    exit();
 }
 
//CREER UN USER lors de l'inscription
 //sauvegarder le token
 $token = bin2hex(random_bytes(20));
 //créée avec les valeurs du formulaire récupérées en mettant null admin et actif
 $user = User::createUser($nom, $email, password_hash($password, PASSWORD_DEFAULT), 0, 0, $token);
/*  var_dump($user); */

 ////SAUVEGARDER LE USER
 //récupérer l'ID
 $idUser = $database->createUser($user);

 /////ENVOYER L EMAIL D ACTIVATION
if($idUser){
   //message, sujet: buffer, inclure, vider
   ob_start();
   include("../vues/emails/activation.php");
   $message = ob_get_clean();
   $sujet = "Activer votre compte Omega";
   //pour que le message s'affiche en html - pour donner une adresse d'expéditeur
   $headers = "Content-type: text/html; charset=utf-8" . "\r\n";
   $headers .= "From: noreply@club-omega.ch";
   //envoi de l'email
   mail($email, $sujet, $message, $headers);
   //redirection page login avec un message
   $_SESSION["info"] = "Votre inscription a bien été prise en compte,
   vous avez reçu un mail pour activer votre compte";
   header("location: ../vues/login.php");
}else{
   //si une erreur s' est produite
   $_SESSION["error"] = "Une erreur s'est produite lors de votre inscription,
   veuillez recommencer le processus";
   header("location: ../vues/inscription.php");
   }
?>