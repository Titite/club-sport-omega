<?php
//activation du user
session_start();
require_once(__DIR__ ."/../models/Database.php");
$database = new Database();

//données récupérées
$idUser = isset($_GET["id"]) ? $_GET["id"] : null;
$token = isset($_GET["token"]) ? $_GET["token"] : null;

//vérifications
$user = $database->getUserById($idUser);
if($idUser == null || $token == null){
    $_SESSION["error"] = "Un problème est survenu lord de votre inscription, veuillez recommencer.";
    header("location: ../vues/inscription.php");
    exit(); //données bien postées
}
if(!$user){
    $_SESSION["error"] = "Un problème est survenu lord de votre inscription, veuillez recommencer.";
    header("location: ../vues/inscription.php");
    exit(); //user retrouvé
}
if($token != $user->getToken()){
    $_SESSION["error"] = "Un problème est survenu lord de votre inscription, veuillez recommencer.";
    header("location: ../vues/inscription.php");
    exit(); //token semblables
}

//activation!
if($database->activateUser($idUser)){
    $_SESSION["info"] = "Votre compte a été activé, vous pouvez vous connecter";
    header("location: ../vues/login.php"); //connexion possible mtn
}else{
    $_SESSION["error"] = "Un problème est survenu lord de votre inscription, veuillez recommencer.";
    header("location: ../vues/inscription.php"); //ou erreur
}