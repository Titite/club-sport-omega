<?php
session_start();
require_once(__DIR__ ."/../models/Database.php");
$database = new Database;

$idSeance = $_GET["id"];
$idUser = $_SESSION["id"];

if($database->deleteParticipant($idSeance, $idUser)){
    $_SESSION["info"] = "Vous avez bien été désinscrit de cette séance";
}else{
    $_SESSION["error"] = "Nous n'avons pas réussi à vous désinscrire de cette séance";
}
header("location: ../vues/cours.php?id=".$idSeance);
exit();
?>