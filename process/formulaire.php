<?php
//////ACTION POUR LE SERVEUR - PROCESSER LES DONNEES
///TRANSFERER LES MESSAGES PAGES-PAGES
//DEBUT DES PAGES
session_start(); //démarrage de la session(?quelle session?)
require_once(__DIR__ . "/../models/Database.php");
$database = new Database(); //pour futures interactions avec BDD

/* 3 CONSTANTES POSSIBLES */
const CREATION = 1;
const MODIFICATION = 2;
const DUPLICATION = 3;
//action differentes en fonction de.
//pour l'instant tout en commun

//RECUPERER DONNEES DU FORMAULAIRE
//attribut de $_POST = champ name="" du formulaire
//isset : vérification si l'attribut est bien présent dans le formulaire - isset($_POST[""])) ?
//si isset ok ->execute la partie avant : - $_POST[""] =="mettre la valeur récupérée dans l'attribut
$titre = isset($_POST["titre"]) ? $_POST["titre"] : "";
$description = isset($_POST["description"]) ? $_POST["description"] : "Séance de sport";
$heureDebut = isset($_POST["heureDebut"]) ? $_POST["heureDebut"] : "00:00";
$date = isset($_POST["date"]) ? $_POST["date"] : date("Y-m-d");
$duree = isset($_POST["duree"]) ? $_POST["duree"] : 45;
$nbParticipantsMax = isset($_POST["nbParticipantsMax"]) ? $_POST["nbParticipantsMax"] : 20;
$couleur = isset($_POST["couleur"]) ? $_POST["couleur"] : "#ffffff";
$materiel = isset($_POST["materiel"]) ? $_POST["materiel"] : "tenue de sport";
$tarif = isset($_POST["tarif"]) ? $_POST["tarif"] : "10";

$image = isset($_POST["image"]) ? $_POST["image"] : "image5.jpg";
$type = isset($_POST["type"]) ? $_POST["type"] : CREATION;
$id = isset($_POST["id"]) ? $_POST["id"] : null;
/* var_dump($_POST); */

//CREER UNE SEANCE AVEC DONNEES RECUES
//créée avec les valeurs du formulaire récupérées
$seance = Seance::createSeance($titre, $description, $heureDebut, $date, $duree, $nbParticipantsMax, $couleur, $materiel, $tarif);
//initialisation pour recevoir les messages
$error = null;
$succes = null;

//////ACTION PAR LE SERVEUR
//Créer ou modifier la séance en fonction du cas
//switch dispactche le comportement de la commande en fonction d'un paramètre (type)
switch ($type) {
    case CREATION:
    case DUPLICATION:
        //Creation de new seance
        $id = $database->createSeance($seance);
        if ($id) {
            $succes = "La séance a été crée avec succès";
        } else {
            $error = "la création de la séance a rencontré une erreur";
        }
        break;
    case MODIFICATION:
        //id nécessaire car nouvel id envoyé via le champ hidden du formulaire
        $seance->setId($id);
        if ($database->updateSeance($seance)) {
            $succes = "La séance a été modifiée avec succès";
        } else {
            $error = "La modification de la séance a rencontré une erreur";
        }
    default:
        //on se fait rien
}
//////REDIRECTIONS
if ($error == null) {
    //tout ok - message ok - direction page vues seance avec la nouvelle affichée
    $_SESSION["info"] = $succes;
    header("Location: ../vues/cours.php?id=" . $id);
} else {
    $_SESSION["info"] = $error;
    //redirection du surfer vers le formulaire malrempli
    header("Location: ../vues/formulaire.php?id" . $id . "&type=" . $type);
}
