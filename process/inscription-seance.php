<?php
session_start();
require_once(__DIR__ ."/../models/Database.php");
$database = new Database;

//récuperer l'id de la seance dans l'url, puis l'id du user dans la session
$idSeance = $_GET["id"];
$idUser = $_SESSION["id"];
$id = isset($_POST["id"]) ? $_POST["id"] : null;

//inscription
if($database->insertParticipant($idSeance, $idUser)){
    //si ok
    $_SESSION["info"] = "Vous avez bien été inscrit à cette séance";
}
else if($database->isInscritExists($idUser, $idSeance)){
    $_SESSION["info"] = "Vous êtes déjà inscrit!";
}else{
    $_SESSION["error"] = "nous n'avons pas réussi à vous inscrire à cette séance";
}
/* var_dump($_SESSION) */
//redirection
header("location: ../vues/cours.php?id=".$idSeance);
exit();
?>