<?php
//SUPPRESSION D UNE SEANCE
//passages page/page
session_start();

require_once(__DIR__ ."/../models/Database.php");
$database = new Database();

//id de l'url
$idSeance = $_GET["id"];
////verification id
//si erreur de récupération d'ID, retour a la page precedente avec message d'erreur
if(!$idSeance){
    $_SESSION["error"] = "Le lien de suppression n'est pas correct";
    header("location: ../vues/planning.php");
}
//si ok -> suppression
if($database->deleteSeance($idSeance)){
    $_SESSION["info"] = "Séance supprimée avec succès";
    header("location: ../vues/planning.php");
}else{
    $_SESSION["error"] = "Le lien de suppression n'est pas correct";
    header("location: ../vues/cours.php?id=".$id);
}
