USE clubOmega;

/* TABLE DES SEANCES */
/* (id, titre, descritption, heureDébut, date, duree, nbParticipantsMax, couleur, materiel, tarif) */
CREATE TABLE seances (
    id SMALLINT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
    titre VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    description TEXT CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'Seance de sport',
    heureDebut TIME NOT NULL,
    date DATE NOT NULL,
    duree INT NOT NULL DEFAULT "90",
    nbParticipantsMax SMALLINT DEFAULT "20",
    couleur VARCHAR (255) DEFAULT "#ffffff",
    materiel TEXT CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'Tenue de sport',
    tarif INT NOT NULL DEFAULT "10"
);

/* TABLE DES UTILISATEURS */
/* id, nom, email, password, isAdmin, isActif, token */
CREATE TABLE users(
    id SMALLINT UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
    nom VARCHAR (30) NOT NULL,
    email VARCHAR (60) NOT NULL,
    password VARCHAR (40) NOT NULL,
    isAdmin TINYINT NOT NULL,
    isActif TINYINT NOT NULL,
    token VARCHAR(255)
);

/* TABLE DE CONNEXIONS AVEC LES INSCRITS */
/* les apostrophes inversés car mot clés, mais lequel? id??
 ca a marché grace a ca, puis mtn je peux sans, je comprends pas */
CREATE TABLE inscrits (
    id_seance SMALLINT UNSIGNED,
    `id_user` SMALLINT UNSIGNED,
    PRIMARY KEY (`id_seance`, `id_user`),
    FOREIGN KEY(`id_seance`) REFERENCES seances(id),
    FOREIGN KEY(`id_user`) REFERENCES users(id)
);