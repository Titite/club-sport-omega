CREATE DATABASE clubOmega;

CREATE USER 'adminClub' @'%' IDENTIFIED BY 'qwertz1234';

GRANT ALL PRIVILEGES ON clubOmega.* TO 'adminClub' @'%';