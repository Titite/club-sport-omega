<?php
include "modules/partie1.php";
?>
<section id="haut">
    <div class="container-fluid mr-5 p-5">
        <div class="row d-flex justify-content-center">
            <div class="card col-lg-8 pl-0">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="/vues/assets/images/image3c.jpg" class="card-img" alt="plusieurs figures de gym/parkour">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">Inscription</h5>
                            <form action="../process/inscription.php" method="POST">
                                <!-- <div class="form-row">
                                <div class="col">
                                    <input type="text" class="form-control" placeholder="First name">
                                </div>
                                <div class="col">
                                    <input type="text" class="form-control" placeholder="Last name">
                                </div>
                            </div> -->
                                <div class="form-column">
                                    <div class="form-group col-md-12">
                                        <label for="name">Nom</label>
                                        <input type="name" class="form-control" id="name" name="name" placeholder="Nom" required>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="password">Mot de passe</label>
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe" required>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="password-repeat">Retaper le mot de passe</label>
                                        <input type="password" class="form-control" id="password-repeat" name="password-repeat" placeholder="Retaper le mot de passe" required>
                                    </div>
                                </div>
                                <!-- 
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="gridCheck">
                                    <label class="form-check-label" for="gridCheck">
                                        Keep me connected!
                                    </label>
                                </div>
                            </div> -->
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary px-5">S'inscrire</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "modules/partie3.php";
?>