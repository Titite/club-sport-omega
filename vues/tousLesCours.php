<?php
include "modules/partie1.php";
?>
<?php
require_once(__DIR__ . "/../models/Database.php");
//index associés aux jours "W" voir doc php
const LUNDI = 1;
const MARDI = 2;
const MERCREDI = 3;
const JEUDI = 4;
const VENDREDI = 5;
const SAMEDI = 6;

$seances = [];
$seances[LUNDI] = [];
$seances[MARDI] = [];
$seances[MERCREDI] = [];
$seances[JEUDI] = [];
$seances[VENDREDI] = [];
$seances[SAMEDI] = [];

$database = new Database();
$seancesOfWeek = $database->getSeanceByWeek(date("W"));

foreach ($seancesOfWeek as $seance) {
    //determiner le jour
    $indexDay = date("w", strtotime($seance->getDate()));
    //ajout au tableau associé au jour donné
    array_push($seances[$indexDay], $seance);
}
?>
<section class="bg-street-2" id="haut">
    <div class="d-flex bd-highlight" id="tousLesCours">
        <div class="shdw text-white p-5 flex-fill bd-highlight ml-5">
            <h1>Cours de la semaine</h1>
        </div>
    </div>
    <nav class="navbar sticky-top mt-5 pt-5">
        <ul class="nav mx-auto bg-grey">
            <li class="nav-item">
                <a class="nav-link active" href="#lundi">Lundi</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#mardi">Mardi</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#mercredi">Mercredi</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#jeudi">Jeudi</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#vendredi">Vendredi</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#samedi">Samedi</a>
            </li>
        </ul>
    </nav>
    <section class="">
        <div class="text-center tous-cours">
            <div class="d-flex bd-highlight" id="lundi">
                <div class="p-5 flex-fill bd-highlight">
                    <h2>Lundi</h2>
                    <div class="card-body">
                        <?php
                        foreach ($seances[LUNDI] as $seance) {
                            include 'modules/cours-card-1.php';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="d-flex bd-highlight" id="mardi">
                <div class="p-5 flex-fill bd-highlight">
                    <h2>Mardi</h2>
                    <div class="card-body">
                        <?php
                        foreach ($seances[MARDI] as $seance) {
                            include 'modules/cours-card-1.php';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="d-flex bd-highlight" id="mercredi">
                <div class="p-5 flex-fill bd-highlight">
                    <h2>Mercredi</h2>
                    <div class="card-body">
                        <?php
                        foreach ($seances[MERCREDI] as $seance) {
                            include 'modules/cours-card-1.php';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="d-flex bd-highlight" id="jeudi">
                <div class="p-5 flex-fill bd-highlight">
                    <h2>Jeudi</h2>
                    <div class="card-body">
                        <?php
                        foreach ($seances[JEUDI] as $seance) {
                            include 'modules/cours-card-1.php';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="d-flex bd-highlight" id="vendredi">
                <div class="p-5 flex-fill bd-highlight">
                    <h2>Vendredi</h2>
                    <div class="card-body">
                        <?php
                        foreach ($seances[VENDREDI] as $seance) {
                            include 'modules/cours-card-1.php';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="d-flex bd-highlight" id="samedi">
                <div class="p-5 flex-fill bd-highlight">
                    <h2>Samedi</h2>
                    <div class="card-body">
                        <?php
                        foreach ($seances[SAMEDI] as $seance) {
                            include 'modules/cours-card-1.php';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
<?php
include "modules/partie3.php";
?>