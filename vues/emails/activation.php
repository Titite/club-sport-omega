<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- lien CSS Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Activer votre compte Omega</title>
</head>

<body>
    <div class="container-fluid">
        <div class="row d-flex justify-content-center">
            <div class="row card col-lg-5 p-0">
                <img src="/vues/assets/images/image.jpg" class="card-img img-covers" alt="sauts de Parkour">
            </div>
            <div class="row card-body m-2">
                <h1><a class="text-center text-uppercase font-weight-bolder m-5 p-5" href="">Club Omega</a></h1>
                <div class="text-left col-12 m-2">
                    <h2 class="card-title font-weight-bold">Bonjour <?php echo $nom; ?> !</h2>
                    <h3 class="card-subtitle">Bienvenu au Club de sport Omega.</h3>
                    <p><strong>Pour valider ton inscription</strong>, clique sur le bouton de validation ci-dessous.</p>
                    <span class="text-right my-2">A Bientôt!</span>
                    <a type="btn btn-success" href="http://localhost/process/activation.php?id=<?php echo $idUser; ?>
                            &token=<?php echo $token; ?>">Activer mon compte</a>
                </div>
</body>
</html>