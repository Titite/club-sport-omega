<?php
include "modules/partie1.php";
?>
<?php
require_once(__DIR__ . "/../models/Database.php");
//index associés aux jours "W" voir doc php
const LUNDI = 1;
const MARDI = 2;
const MERCREDI = 3;
const JEUDI = 4;
const VENDREDI = 5;
const SAMEDI = 6;

$seances = [];
$seances[LUNDI] = [];
$seances[MARDI] = [];
$seances[MERCREDI] = [];
$seances[JEUDI] = [];
$seances[VENDREDI] = [];
$seances[SAMEDI] = [];

$database = new Database();
$seancesOfWeek = $database->getSeanceByWeek(date("W"));

foreach ($seancesOfWeek as $seance) {
    //determiner le jour
    $indexDay = date("w", strtotime($seance->getDate()));
    //ajout au tableau associé au jour donné
    array_push($seances[$indexDay], $seance);
}
?>
<section class="bg-street" id="haut">
    <div class="container-fluid">
        <!-- dark card -->
        <div class="card bg-secondary mb-3 row col-lg-10 col-12 offset-lg-1 pb-3">
            <div class="card-header text-center">
                <h1>Planning de la semaine</h1>
            </div>
            <div class="container-planning card-group container-fluid">
                <!-- grey card -->
                <div class="card col-lg-2 col-md-4 col-6 p-0">
                    <div class="card bg-light">
                        <div class="card-header text-center text-uppercase">
                            <h2>Lundi</h2>
                        </div>
                        <div class="card-body">
                            <?php
                            foreach ($seances[LUNDI] as $seance) {
                                include 'modules/etiquette.php';
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <!-- grey card -->
                <div class="card col-lg-2 col-md-4 col-6 p-0">
                    <div class="card bg-light">
                        <div class="card-header text-center text-uppercase">
                            <h2>Mardi</h2>
                        </div>
                        <div class="card-body">
                            <?php
                            foreach ($seances[MARDI] as $seance) {
                                include 'modules/etiquette.php';
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <!-- grey card -->
                <div class="card col-lg-2  col-md-4 col-6 p-0">
                    <div class="card bg-light">
                        <div class="card-header text-center text-uppercase">
                            <h2 id="planning-me">Mercredi</h2>
                        </div>
                        <div class="card-body">
                            <?php
                            foreach ($seances[MERCREDI] as $seance) {
                                include 'modules/etiquette.php';
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <!-- grey card -->
                <div class="card col-lg-2  col-md-4 col-6 p-0">
                    <div class="card bg-light">
                        <div class="card-header text-center text-uppercase">
                            <h2>Jeudi</h2>
                        </div>
                        <div class="card-body">
                            <?php
                            foreach ($seances[JEUDI] as $seance) {
                                include 'modules/etiquette.php';
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <!-- grey card -->
                <div class="card col-lg-2  col-md-4 col-6 p-0">
                    <div class="card bg-light">
                        <div class="card-header text-center text-uppercase">
                            <h2>Vendredi</h2>
                        </div>
                        <div class="card-body">
                            <?php
                            foreach ($seances[VENDREDI] as $seance) {
                                include 'modules/etiquette.php';
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <!-- grey card -->
                <div class="card col-lg-2  col-md-4 col-6 p-0">
                    <div class="card bg-light">
                        <div class="card-header text-center text-uppercase">
                            <h2>Samedi</h2>
                        </div>
                        <div class="card-body">
                            <?php
                            foreach ($seances[SAMEDI] as $seance) {
                                include 'modules/etiquette.php';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "modules/partie3.php";
?>