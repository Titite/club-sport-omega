<?php
include "modules/partie1.php";
?>
<!-- SCRIPT PHP FORMULAIRE -->
<?php
require_once(__DIR__ . "/../models/Database.php");

//3 CONSTANTES POSSIBLES
const CREATION = 1;
const MODIFICATION = 2;
const DUPLICATION = 3;

//Récuperation des valeur de l'url
$idSeanceOrigine = $_GET["id"];
$type = $_GET["type"];

/* Récupère la séance d'origine (si exist) */
if (isset($idSeanceOrigine)) {
    $database = new Database;
    $seanceOrigine = $database->getSeanceById($idSeanceOrigine);
}
//Création d'une séance (remplir valeurs inputs)
$seance = new Seance();

$titre = "";
switch ($type) {
    case CREATION:
        //on garde VIDE
        $titre = "Creation d'une séance";
        break;
    case MODIFICATION:
        //on MODIFIE
        $seance = $seanceOrigine;
        $titre = "Modification de ";
        break;
    case DUPLICATION:
        //on COPIE SAUF l'ID
        $seance->setTitre($seanceOrigine->getTitre());
        $seance->setDescription($seanceOrigine->getDescription());
        $seance->setDate($seanceOrigine->getDate());
        $seance->setHeureDebut($seanceOrigine->getHeureDebut());
        $seance->setDuree($seanceOrigine->getDuree());
        $seance->setNbParticipantsMax($seanceOrigine->getNbParticipantsMax());
        $seance->setCouleur($seanceOrigine->getCouleur());
        $seance->setMateriel($seanceOrigine->getMateriel());
        $seance->setTarif($seanceOrigine->getTarif());
        $titre = "Duplication de ";
        break;
    default:
        //par défault - VIDE (pour création)
        $titre = "Aucun";
}
?>
<!-- STRUCTURE DU FORMULAIRE VUES HTML -->
<div class="card container-fluid">
    <h1 class="card-header text-uppercase font-weight-bold row justify-content-center"><?php echo $titre . $seance->getTitre() ?></h1>
    <div class="card-body row">
        <form class="offset-md-1 col-md-10 col-12" action="../process/formulaire.php" method="POST">
            <input type="hidden" name="type" value="<?php echo $type ?>" />
            <input type="hidden" name="id" value="<?php echo $seance->getId() ?>" />
            <div class="form-group row">
                <label for="titre" class="col-sm-2 col-form-label">Titre</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="titre" placeholder="Nom du cours" name="titre" value="<?php echo $seance->getTitre() ?>" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="description" class="col-sm-2 col-form-label">Description</label>
                <div class="col-sm-10">
                    <textarea class="form-control" id="description" name="description">
                        <?php echo $seance->getDescription() ?>
                    </textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="date" class="col-sm-2 col-form-label">Date</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" id="date" placeholder="Date du cours" name="date" value="<?php echo $seance->getDate() ?>" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="heureDebut" class="col-sm-2 col-form-label">Heure de début</label>
                <div class="col-sm-10">
                    <input type="time" class="form-control" id="heureDebut" name="heureDebut" value="<?php echo $seance->getHeureDebut() ?>" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="duree" class="col-sm-2 col-form-label">Durée (min)</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" id="duree" name="duree" placeholder="90" value="<?php echo $seance->getDuree() ?>" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="nbParticipantsMax" class="col-sm-2 col-form-label">Nombre de participants max</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" id="nbParticipantsMax" name="nbParticipantsMax" placeholder="20" value="<?php echo $seance->getNbParticipantsMax() ?>" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="couleur" class="col-sm-2 col-form-label">Couleur de fond</label>
                <div class="col-sm-10">
                    <input type="color" class="form-control" id="couleur" placeholder="Couleur" name="couleur" value="<?php echo $seance->getCouleur() ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="materiel" class="col-sm-2 col-form-label">Matériel nécessaire</label>
                <div class="col-sm-10">
                    <textarea class="form-control" id="materiel" name="materiel" placeholder="tenue de sport"><?php echo $seance->getMateriel() ?></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="tarif" class="col-sm-2 col-form-label">Tarif</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" id="tarif" name="tarif" placeholder="10" value="<?php echo $seance->getTarif() ?>" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="image" class="col-sm-2 col-form-label">Choisir une image</label>
                <div class="col-sm-10">
                    <input type="file" disabled="disabled" id="image" name="image" accept="image/png, image/jpeg">
                </div>
            </div>

            <div class="form-group text-center">
                <button type="submit" class="btn btn-dark px-5 text-uppercase">
                    <?php switch ($type) {
                        case CREATION:
                            echo "Créer";
                            break;
                        case MODIFICATION:
                            echo "Modifier";
                            break;
                        case DUPLICATION:
                            echo "Dupliquer";
                            break;
                    }
                    ?>
                </button>
            </div>
        </form>
    </div>
</div>
<?php
include "modules/partie3.php";
?>