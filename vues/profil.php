<?php
include "modules/partie1.php";
?>
<?php
require_once(__DIR__ . "/../models/Database.php");
$database = new Database;
//Restrictions
//s'il n'est pas connecté - direction au login
$user = unserialize($_SESSION["user"]); //info user
$seances = $database->getSeanceByUserId($user->getId()); //seances inscrites du user
?>
<section class="bg-street" id="haut">
    <div class="card-profil mr-5 p-5">
        <div class="row d-flex justify-content-center">
            <div class="card offset-md-1 col-md-10 col-12 px-0">
                <div class="row no-gutters">
                    <div class="col-lg-2 col-2">
                        <img src="/vues/assets/images/image3b.jpg" class="card-img img-covers" alt="en dessin: un homme portant une dague sur un toit très en hauteur observant la terrasse du batiment d'en face (décor oriental).">
                    </div>
                    <div class="col-lg-10 col-10 d-flex justify-content-center align-items-top">
                        <div class="card-body container-fluid">
                            <div class="row text-center">
                                <div class=" col-12 my-5">
                                    <!-- <h2 class="card-title font-weight-bold">Pseudo</h2> -->
                                    <h3 class="card-subtitle"><?php echo $user->getNom(); ?></h3>
                                </div>
                            </div>
                            <div class="row text-left">
                                <div class="col-12 col-md-8 offset-md-4 my-5">
                                    <h4 class="card-title font-weight-bold">Cours</h4>
                                    <h5 class="card-title font-weight-bold">Vous êtes inscrit au cours suivants :</h5>
                                    <ul class="inscrit">
                                        <?php foreach ($seances as $seance) { ?>
                                            <li><i class="fas fa-running mx-3"></i>
                                                <a href="/vues/cours.php?id=<?php echo $seance->getId() ?>">
                                                    <?php echo $seance->getTitre() . ", le " . date("d/m/Y", strtotime($seance->getDate())) . " à " . date("G\hi", strtotime($seance->getHeureDebut())) ?>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="row text-left">
                                <div class="col-12 col-md-8 offset-md-4 mb-5">
                                    <h4 class="card-title font-weight-bold">Abonnements</h4>
                                    <ul class="card-subtitle">Vous n'avez pas souscrit d'abonnement pour l'instant.</ul>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="d-flex flex-row justify-content-center my-3">
                                    <a href="/vues/modifier-profil.php?id=<?php echo $user->getId(); ?>" disabled="disabled" class="sebshdw btn btn-secondary mx-2">Modifier</a>
                                    <a href="/process/deconnexion.php" class="btn btn-danger mx-2">Se déconnecter</a>
                                    <a href="" disabled="disabled" class="sebshdw btn btn-secondary mx-2">Supprimer le compte</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "modules/partie3.php";
?>