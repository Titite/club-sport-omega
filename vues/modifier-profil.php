<?php
include "modules/partie1.php";
?>

<?php
require_once(__DIR__ . "/../models/Database.php");

//Récuperation des valeur de l'url
$idUserOrigine = $_GET["id"];

/* Récupère la séance d'origine (si exist) */
if (isset($idUserOrigine)) {
    $database = new Database;
    $userOrigine = $database->getUserById($idUserOrigine);

    $user = new User();
    //on MODIFIE
    $user = $userOrigine;
    $titre = "Modification du Profil de ";
}
?>
<section id="haut">
    <h1 class="text-center"><?php echo $titre . $user->getNom($user) ?></h1>
    <div class="card-profil mr-5 p-5">
        <div class="row d-flex justify-content-center">
            <div class="card offset-md-1 col-md-10 col-12 px-0">
                <div class="row no-gutters">
                    <div class="col-lg-2 col-2">
                        <img src="/vues/assets/images/image3b.jpg" class="card-img img-covers" alt="en dessin: un homme portant une dague sur un toit très en hauteur observant la terrasse du batiment d'en face (décor oriental).">
                    </div>
                    <div class="col-lg-10 col-10 d-flex justify-content-center align-items-top text-uppercase">
                        <form class="card-body container-fluid" action="../process/modifier-profil.php" method="POST">
                            <input type="hidden" name="id" value="<?php echo $user->getId() ?>" />
                            <div class="row text-center font-weight-bold">
                                <div class="col-12 col-md-8 offset-md-3 my-5">
                                    <div class="form-group col-md-10">
                                        <label for="name">Modifier le nom :</label>
                                        <input type="text" class="form-control text-center" id="name" name="nom" placeholder="Nom" value="<?php echo $user->getNom() ?>" required>
                                    </div>
                                </div>
                            </div>
                            <div class=" row text-left font-weight-bold">
                                <div class="col-12 col-md-8 offset-md-3 my-5">
                                    <div class="form-group col-md-10">
                                        <label for="email">Modifier l'email :</label>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $user->getEmail() ?>" required>
                                    </div>
                                    <div class=" form-group col-md-10 mt-5">
                                        <label for="password">Confirmer avec le mot de passe :</label>
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe" required>
                                    </div>
                                    <div class=" form-group col-md-10">
                                        <label for="password-repeat">Retaper le mot de passe :</label>
                                        <input type="password" class="form-control" id="password-repeat" name="password-repeat" placeholder="Retaper le mot de passe" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-groupe text-center offset-lg-1">
                                <button type="submit" class="btn btn-primary px-5">Valider</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "modules/partie3.php";
?>