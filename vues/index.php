<?php
include "modules/partie1.php";
?>

<main class="wrapper">

  <section id="haut">
    <nav class="navbar sticky-top">
      <ul class="nav ml-5 pl-3" id="nav-index">
        <li class="nav-item">
          <a class="nav-link active" href="#le-club">Le Club</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#club-histoire">Notre Histoire</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#club-profs">Nos profs</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#club-galerie" tabindex="-1" aria-disabled="true">Galerie</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#club-events" tabindex="-1" aria-disabled="true">Nos Events</a>
        </li>
      </ul>
    </nav>
    <section class="section parallax bg1" id="le-club">
      <h1>LE CLUB OMEGA</h1>
    </section>
    <section class="section static">
      <p>Ce Club est un club qui promouvoit de nombreuses disciplines sportives, autant outdoor que indoor. Les principales disciplines enseignées sont le parkour, l'acrobatie et le fitness, cependant tous sports sont les bienvenus : nous acceuillons actuellement une classe de gymnastique acrobatique et de yoga. Toute proposition ou demande qui respecte notre philosophie est acceuillie à bras ouvert. L'objectif des créateurs du Club est faire connaître le plaisir de bouger son corps, ainsi que les vastes possibilités qu'offre les capacités du corps humain pour se mouvoir. C'est à travers l'art, le sport et la découverte personnelle de soi, que le Club Omega poursuit son but ultime : permettre l'épanouissement de chacun à travers un sport, quel qu'il soit.</p>
    </section>
    <section class="section parallax bg2" id="club-histoire">
      <h1>NOTRE HISTOIRE</h1>
    </section>
    <section class="section static">
      <p>Tout a commencé par une rencontre entre bandes de potes, fans de parkour:
        les petits groupes devenaient plus grands et on s'est faits remarqués par des jeunes du quartier,
        motivés à commencer la sport. Ils nous demandaient des conseils et des techniques pour progresser, éviter de se faire mal,
        rester prudent ..et on leurs donnait des tuyaux. Sans qu'on s'en rendent compte, on commençait à leur donner des
        sortes de cours et on a pris plaisir à les rencontrer chaque semaine. Ils nous en étaient très reconnaissants.
        Finalement l'un d'entre nous eu une salle à disposition et c'est là qu'on a penser
        à ouvrir un club pour ces jeunes. Apprendre à d'autres notre passion nous épanouit et
        nous apporte aussi beaucoup d'inspiration .. et parce que le sport a le pouvoir procurer le sourire aux gens,
        le Club de Sport Oméga a vu le jour.</p>
    </section>
    <section class="section parallax bg3 text-dark" id="club-profs">
      <h1>LES PROFS</h1>
    </section>
    <section class="section static p-0">
      <div class="card-profil col-12 py-4">
        <div class="row flex-wrap d-flex justify-content-center align-items-center">
          <div class="card col-md-4 col-11 px-0 m-3">
            <div class="row no-gutters">
              <div class="col-lg-4 col-4">
                <img src="/vues/assets/images/unnamed.jpg" class="card-img img-covers" alt="un profil avatar non personnalisé">
              </div>
              <div class="col-lg-8 col-8 d-flex justify-content-center align-items-top">
                <div class="card-body container-fluid">
                  <div class="row text-center">
                    <div class=" col-12 my-5">
                      <h1 class="card-subtitle">Prof 1</h1>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card col-md-4 col-11 px-0 m-3">
            <div class="row no-gutters">
              <div class="col-lg-4 col-4">
                <img src="/vues/assets/images/unnamed.jpg" class="card-img img-covers" alt="un profil avatar non personnalisé">
              </div>
              <div class="col-lg-8 col-8 d-flex justify-content-center align-items-top">
                <div class="card-body container-fluid">
                  <div class="row text-center">
                    <div class=" col-12 my-5">
                      <h1 class="card-subtitle">Prof 2</h1>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card col-md-4 col-11 px-0 m-3">
            <div class="row no-gutters">
              <div class="col-lg-4 col-4">
                <img src="/vues/assets/images/unnamed.jpg" class="card-img img-covers" alt="un profil avatar non personnalisé">
              </div>
              <div class="col-lg-8 col-8 d-flex justify-content-center align-items-top">
                <div class="card-body container-fluid">
                  <div class="row text-center">
                    <div class=" col-12 my-5">
                      <h1 class="card-subtitle">Prof 3</h1>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="section parallax bg4 text-dark" id="club-galerie">
      <h1>LA GALERIE</h1>
    </section>
    <section class="section static p-0">
      <div class="card-profil col-12 px-2 py-4 my-5">
        <div class="row flex-wrap d-flex justify-content-center align-items-center">
          <div class="card col-md-3 col-4 px-0 m-3">
            <img src="https://vide-greniers.org/images/picture/traitementPhoto.png" class="card-img img-covers" alt="un profil avatar non personnalisé">
          </div>
          <div class="card col-md-3 col-4 px-0 m-3">
            <img src="https://vide-greniers.org/images/picture/traitementPhoto.png" class="card-img img-covers" alt="un profil avatar non personnalisé">
          </div>
          <div class="card col-md-3 col-4 px-0 m-3">
            <img src="https://vide-greniers.org/images/picture/traitementPhoto.png" class="card-img img-covers" alt="un profil avatar non personnalisé">
          </div>
          <div class="card col-md-3 col-4 px-0 m-3">
            <img src="https://vide-greniers.org/images/picture/traitementPhoto.png" class="card-img img-covers" alt="un profil avatar non personnalisé">
          </div>
          <div class="card col-md-3 col-4 px-0 m-3">
            <img src="https://vide-greniers.org/images/picture/traitementPhoto.png" class="card-img img-covers" alt="un profil avatar non personnalisé">
          </div>
          <div class="card col-md-3 col-4 px-0 m-3">
            <img src="https://vide-greniers.org/images/picture/traitementPhoto.png" class="card-img img-covers" alt="un profil avatar non personnalisé">
          </div>
        </div>
      </div>
    </section>
    <section class="section parallax bg5" id="club-events">
      <h1>NOS EVENTS</h1>
    </section>
    <section class="section static p-0">
      <div class="card-profil col-12 px-2 py-4 my-5">
        <div class="row flex-wrap d-flex justify-content-center align-items-center">
          <div class="card col-md-3 col-4 px-0 m-3">
            <img src="/vues/assets/images/image10.jpg" class="card-img img-covers" alt="un article">
          </div>
          <div class="card col-md-3 col-4 px-0 m-3">
            <img src="/vues/assets/images/image10.jpg" class="card-img img-covers" alt="un article">
          </div>
          <div class="card col-md-3 col-4 px-0 m-3">
            <img src="/vues/assets/images/image10.jpg" class="card-img img-covers" alt="un article">
          </div>
        </div>
      </div>
    </section>

  </section>
</main>
<?php
include "modules/partie3.php";
?>