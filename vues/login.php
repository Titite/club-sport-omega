<?php
include "modules/partie1.php";
?>
<section id="haut">
    <div class="container-fluid">
        <div class="row d-flex justify-content-center">
            <div class="card col-lg-5 p-0">
                <div id="img-login" class="card-img-top" alt="sauts de Parkour"></div>
                <div class="card-body m-2">
                    <h5 class="card-title">Connexion</h5>
                    <form action="../process/login.php" method="POST">
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" required>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" placeholder="Password" name="password" required>
                        </div>
                        <!-- <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="checkbox">
                        <label class="form-check-label" for="checkbox">Keep me connected</label>
                    </div> -->
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
</section>
<?php
include "modules/partie3.php";
?>