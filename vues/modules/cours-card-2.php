
<div class="card-cours container-fluid mr-5 p-5">
    <div class="row d-flex justify-content-center">
        <div class="card col-lg-8 col-md-10 col-12 px-0">
            <div class="row no-gutters">
                <div class="col-lg-8 col-10 d-flex justify-content-center align-items-center">
                    <div class="card-body container-fluid" style="background: <?php echo $seance->getCouleur() ?>;">
                        <div class="row text-center pb-2">
                            <div class="col-12 my-2">
                                <h2 class="card-title font-weight-bold"><?php echo $seance->getTitre() ?></h2>
                                <!--                                 <h3 class="card-subtitle">discipline</h3> -->
                            </div>
                            <div class="col-12 d-flex flex-row justify-content-center">
                                <span class="card-text py-1 mx-4">Date: <?php echo $seance->getDate() ?></span>
                                <span class="card-text py-1 mx-4">Heure: <?php echo $seance->getHeureDebut() ?></span>
                                <span class="card-text py-1 mx-4">Durée: <?php echo $seance->getDuree() ?>minutes</span>
                            </div>
                            <p class="col-12 py-1"><?php echo $seance->getDescription() ?></p>
                            <div class="col-12">
                                <div class="d-flex flex-row justify-content-center">
                                    <span class="card-text py-1">Nombre de participants max : <?php echo $seance->getNbParticipantsMax() ?></span>
                                    <span class="card-text py-1 mx-4">Tarif: <?php echo $seance->getTarif() ?></span>
                                </div>
                                <div class="col-12">
                                    <div class="d-flex flex-row justify-content-center">
                                        <span class="card-text py-1 mx-4">Matériel nécessaire: <?php echo $seance->getMateriel() ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="d-flex flex-row justify-content-center m-2">
                                <a href="" class="btn btn-danger mx-2">Se désinscrire</a>
                                <a href="" class="btn btn-primary mx-2">S'inscrire</a>
                                <a href="" class="btn btn-danger mx-2">Complet</a>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="d-flex flex-row justify-content-center m-2">
                                <a href="/vues/formulaire.php?id=<?php echo $seance->getId(); ?>&type=3" class="btn btn-warning mx-2">Dupliquer</a>
                                <a href="/vues/formulaire.php?id=<?php echo $seance->getId(); ?>&type=2" class="btn btn-primary mx-2">Modifier</a>
                                <a href="/vues/planning.php" class="btn btn-danger mx-2">Supprimer</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-2">
                    <img src="/vues/assets/images/image6.jpg" class="card-img img-cover" alt="trois figures de gymnastique en ombre sur fond blanc(femme)">
                </div>
            </div>
        </div>
    </div>
</div>