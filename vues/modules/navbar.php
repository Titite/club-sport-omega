<?php
require_once(__DIR__ . "/../../models/User.php");
//recup. user
$user = isset($_SESSION["user"]) ? unserialize($_SESSION["user"]) : null;
?>

<nav class="navbar navbar-expand-lg navbar-light bg-light degrade-to-bottom sticky-top" id="navbar-header">
    <a class="navbar-brand text-uppercase font-weight-bolder" href="/vues/index.php">Club Omega</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-5">
            <?php if ($user == null) { ?>
                <li class="nav-item">
                    <a class="nav-link ml-4" href="/vues/login.php">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link ml-4" href="/vues/inscription.php">Inscription</a>
                </li>
            <?php } //end if 
            ?>
            <li class="nav-item">
                <a class="nav-link ml-4" href="/vues/planning.php">Planning</a>
            </li>
            <li class="nav-item">
                <a class="nav-link ml-4" href="/vues/tousLesCours.php">Description des cours</a>
            </li>
            <li class="nav-item">
                        <a class="nav-link ml-4" href="/vues/contact.php">Contact</a>
                    </li>
            <!-- <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle ml-4" disabled="disabled" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Cours
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/vues/tousLesCours.php">Description des cours</a>
                    <a class="dropdown-item" href="/vues/planning.php">Planning</a>
                </div>
            </li> -->
            <?php if ($user != null) { ?>
                <?php if ($user->isAdmin() == 1) { ?>
                    <li class="nav-item">
                        <a class="nav-link ml-4" href="/vues/formulaire.php?type=1">Créer cours</a>
                    </li>
                <?php } //end if 
                ?>
                <?php if ($user->isActif() == 1) { ?>
                    <li class="nav-item">
                        <a class="nav-link ml-4" href="/vues/profil.php">Profil</a>
                    </li>
                <?php } //end if 
                 ?>
                 
                 <?php if ($user->isActif() == 1) { ?>
                    <li class="nav-item">
                        <a class="nav-link ml-4" href="/process/deconnexion.php">Deconnexion</a>
                    </li>
                    <?php } //end if 
                 ?>
            <?php } //end-if 
            ?>
        </ul>
    </div>
</nav>