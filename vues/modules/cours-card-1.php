<section id="haut">
    <div class="card-cours container-fluid mr-5 p-5">
        <div class="row d-flex justify-content-center">
            <div class="card col-lg-8 col-md-10 col-12 px-0">
                <div class="row no-gutters">
                    <div class="col-lg-4 col-2">
                        <img src="/vues/assets/images/image5.jpg" class="card-img img-cover" alt="un saut de parkour visible en 3 ombres en arret-image">
                    </div>

                    <div class="col-lg-8 col-10 d-flex justify-content-center align-items-center">
                        <div class="card-body container-fluid" style="background: <?php echo $seance->getCouleur() ?>">

                            <div class="row text-center pb-2">
                                <div class="col-12 my-2">
                                    <h1 class="card-title font-weight-bold"><?php echo $seance->getTitre(); ?></h1>
                                    <!--                                 <h3 class="card-subtitle">discipline</h3> -->
                                </div>
                                <div class="col-12 d-flex flex-row justify-content-center">
                                    <span class="card-text py-1 mx-4">Date: <?php echo $seance->getDate(); ?></span>
                                    <span class="card-text py-1 mx-4">Heure: <?php echo $seance->getHeureDebut(); ?></span>
                                    <span class="card-text py-1 mx-4">Durée: <?php echo $seance->getDuree(); ?>minutes</span>
                                </div>
                                <p class="col-12 py-1 font-weight-bold"><?php echo $seance->getDescription(); ?></p>
                                <div class="col-12">
                                    <div class="d-flex flex-row justify-content-center">
                                        <span class="card-text py-1">Nombre de participants max : <?php echo $seance->getNbParticipantsMax(); ?></span>
                                        <span class="card-text py-1 mx-4">Tarif: <?php echo $seance->getTarif(); ?></span>
                                    </div>
                                    <div class="col-12">
                                        <div class="d-flex flex-row justify-content-center">
                                            <span class="card-text py-1 mx-4">Matériel nécessaire: <?php echo $seance->getMateriel(); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="d-flex flex-row justify-content-center m-2">
                                    <a class="btn btn-danger mx-2" href="../process/desinscription-seance.php?id=<?php echo $seance->getId(); ?>">Se désinscrire</a>
                                    <a class="btn btn-primary mx-2" href="../process/inscription-seance.php?id=<?php echo $seance->getId(); ?>">S'inscrire</a>
                                    <a class="btn btn-danger mx-2 sebshdw" disabled="disabled btn-secondary">Complet</a>
                                </div>
                            </div>
                            <?php if ($user && $user->isAdmin() == 1) { ?>
                                <!-- si il y a un user et qu il est Admin -->
                                <div class="col-12">
                                    <div class="d-flex flex-row justify-content-center m-2">
                                        <a class="btn-admin btn btn-warning mx-2" href="/vues/formulaire.php?id=<?php echo $seance->getId(); ?>&type=3">Dupliquer</a>
                                        <a class="btn-admin btn btn-primary mx-2" href="/vues/formulaire.php?id=<?php echo $seance->getId(); ?>&type=2">Modifier</a>
                                        <a class="btn-admin btn btn-danger mx-2" href="/process/delete-seance.php?id=<?php echo $seance->getId(); ?>">Supprimer</a>
                                    </div>
                                </div>
                            <?php } //end-if 
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>