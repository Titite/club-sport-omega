<footer>
  <div class="container-fluid p-0">
    <div class="row mr-0 card-header d-flex flex-row justify-content-center" id="footer-top">
      <titre class="text-center text-uppercase font-weight-bold">Club de sport Omega</titre>
    </div>
    <blockquote class=" m-0 p-0 pb-3 px-5 " id="bg-quote">
      <div class="px-5 d-flex bd-highlight text-uppercase mb-0">
        <div class="p-2 w-100 mx-5 px-5" id="quote">Simplicity is the ultimate form of sophistication.</div>
        <footer class="p-2 flex-shrink-1 bd-highlight blockquote-footer text-right">Leodarno Da Vinci</footer>
      </div>
      <div class="row d-flex flex-row text-center justify-content-center">
        <a href="/vues/contact.php" class="btn btn-secondary mx-3 col-lg-2 px-5">Boîte à idées</a>
        <a href="/vues/contact.php" class="btn btn-secondary mx-3 col-lg-2 px-5">Contacte-nous!</a>
        <a href="#haut" class="btn btn-secondary mx-3 col-lg-2 px-5">Haut de page</a>
      </div>
    </blockquote>
  </div>
</footer>