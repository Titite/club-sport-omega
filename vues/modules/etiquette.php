<div class="card etiquetteCours" style="width: 18rem; background: <?php echo $seance->getCouleur() ?>;">
  <a class="card-link" href="/vues/cours.php?id=<?php echo $seance->getId() ?>;">
    <div class="card-body">
      <div class="text-center pb-1">
        <h5 class="card-title font-weight-bold"><?php echo $seance->getTitre() ?> </h5>
        <p class="card-text py-1">
          <?php echo date("G\hi", strtotime($seance->getHeureDebut())) . ", " . $seance->getDuree() . "min"; ?>
        </p>
      </div>
      <div class="d-flex flex-row justify-content-center">
        <a href="/vues/cours.php?id=<?php echo $seance->getId() ?>;" class="card-link mx-4">Détails</a>
        <a href="../process/inscription-seance.php?id=<?php echo $seance->getId(); ?>" class="card-link mx-4">S'inscrire</a>
      </div>
    </div>
  </a>
</div>