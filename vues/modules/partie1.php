<?php
//Session utilisée pour véhiculer des messages d'une page à l'autre
//=>démarrer la seesion en début de page (attention : aucun espace en plus!!)
session_start();
?>

<!DOCTYPE html>
<html lang="fr">
<?php
    include('header.php');
?>

<body>  
<?php
    include('navbar.php');
    include('messages.php');
?>
