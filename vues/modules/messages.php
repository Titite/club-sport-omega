<?php if (isset($_SESSION["error"])) {
    //si erreur
?>
    <div class="alert alert-danger" role="alert">
        <strong><?php echo $_SESSION["error"]; ?></strong>
    </div>
<?php
    //vider les erreur
    $_SESSION["error"] = null;
}
?>
<?php if (isset($_SESSION["info"])) {
    //si info ...afficher:
?>
    <div class="alert alert-success" role="alert">
        <strong><?php echo $_SESSION["info"]; ?></strong>
    </div>
<?php
    //puis vider les infos :
    $_SESSION["info"] = null;
}
