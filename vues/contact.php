<?php
include "modules/partie1.php";
?>
<section class="bg-street" id="haut">
    <div class="card-profil mr-5 p-5">
        <div class="row d-flex justify-content-center">
            <div class="card offset-md-1 col-md-10 col-12 px-0">
                <div class="row no-gutters">
                    <div class="col-lg-8 col-10 d-flex justify-content-center align-items-top">
                        <div class="card-body container-fluid">
                            <div class="row text-center">
                                <div class=" col-12 my-5">
                                    <!-- <h2 class="card-title font-weight-bold">Pseudo</h2> -->
                                    <h1 class="text-uppercase font-weight-bolder" href="/vues/index.php">Club Omega</h1>
                                </div>
                            </div>
                            <div class="contact-cards row d-flex justify-content-center align-items-top flex-wrap">
                                <div class="card border-light m-3 text-center" style="width: 18rem;">
                                    <div class="card-header">Où nous trouver</div>
                                    <div class="card-body">
                                        <h5 class="card-title">Adresse</h5>
                                        <p class="card-text">Avenue de quelque part N°XX</b>XXXXXX, Ville</br>Pays</p>
                                    </div>
                                </div>
                                <div class="card border-light m-3 text-center" style="width: 18rem;">
                                    <div class="card-header"><a>Voir la carte</a></div>
                                    <div class="card-body p-0">
                                        <div style="width: 100%"><iframe width="100%" height="100%" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=montreal+(Our%20location)&amp;ie=UTF8&amp;t=&amp;z=11&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/coordinates.html">gps coordinates</a></iframe></div><br />
                                    </div>
                                </div>
                                <div class="card border-light m-3 text-center" style="width: 18rem;">
                                    <div class="card-header">Comment nous joindre</div>
                                    <div class="card-body">
                                        <h5 class="card-title">Téléphone</h5>
                                        <p class="card-text">+XX XX XXX XX XX</p>
                                    </div>
                                </div>
                                <div class="card border-light m-3 text-center" style="width: 18rem;">
                                    <div class="card-header">Boîte à idées</div>
                                    <div class="card-body">
                                        <h5 class="card-title">Transmet-nous tes suggestions!</h5>
                                        <p class="card-text">emailadmin@boitemail.com</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-2">
                        <img src="/vues/assets/images/image7.jpg" class="card-img img-cover" alt="en dessin: un homme portant une dague sur un toit très en hauteur observant la terrasse du batiment d'en face (décor oriental).">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "modules/partie3.php";
?>